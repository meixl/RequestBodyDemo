# RequestBodyDemo

#### 介绍
@RequestBody 标签使用，入参集合。
搭配 postman 一起使用。

#### 项目说明
后台代码请看`DogController`，如下图：

 <img src="png/222.png" width = "80%" alt="后台入参"/>

前端 postman 入参如下图：

`insert`方法入参：

 <img src="png/insert-入参.png" width = "80%" alt="insert-入参"/>

`insert2`方法入参：

 <img src="png/insert2-入参.png" width = "80%" alt="insert2-入参"/>

#### 图片缩小`markdown`说明
参考知乎高赞回答：[https://www.zhihu.com/question/23378396](https://www.zhihu.com/question/23378396)