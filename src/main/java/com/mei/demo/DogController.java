package com.mei.demo;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dog")
public class DogController {

    @RequestMapping("/time")
    public String time() {
        return LocalDateTime.now().toString();
    }

    @RequestMapping("/insert")
    public List<Dog> insert(@RequestBody List<Dog> list, String address) {
        System.out.println(address);
        return list;
    }

    @RequestMapping("/insert2")
    public DogRequest insert2(@RequestBody DogRequest request) {
        return request;
    }

}