package com.mei.demo;

import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DogRequest {

    List<Dog> dogList;

    String    address;

}