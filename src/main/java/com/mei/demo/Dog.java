package com.mei.demo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Dog {

    private Integer id;

    private String  name;

    private Integer age;

    private String  color;

}